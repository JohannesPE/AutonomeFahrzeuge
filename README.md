# Autonome Fahrzeuge
Hier ist unser Projekt zum Thema Autonomous Vehicle der WPM Text-Mining der Hochschule Flensburg

## Starten des Webservers
```
python server.py 
```
bzw. 
```
py server.py 
```
oder in Anaconda starten

# Git Befehle
## Neusten Stand herunterladen
```
git pull
```

## Änderungen hochladen 
``` 
git add .
git commit -m"HIER WAS IHR GEMACHT HABT"
git push origin master
```