#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 23 17:19:27 2018

@author: johannespe
"""

import spacy
import pandas as pd

#---------------Import---------------------
from bokeh.models import ColumnDataSource, Title, HoverTool
from bokeh.io import output_file, save, show
from bokeh.plotting import figure
from bokeh.colors import RGB
from collections import Counter

class NomenVerbenExtract:
    def getNomen():
        #-STOP WORDS
        nlp = spacy.load('en')
        file = open('files/stopwords.txt','r') 
        for line in file:
           nlp.vocab[line].is_stop = True
        file.close()
        
        
        data = pd.read_csv('files/Organisation.csv', delimiter=',')
        joined_text2 = ' '.join(data.patent_abstract.tolist())
        doc = nlp(joined_text2)
        
        new_string = ''.join([tok.text_with_ws for tok in doc2 if tok.is_stop == False])
        doc2 = nlp(new_string)
        
        token_list_Noun = []
        token_list_Verb = []
        
        for token in doc2:
            if token.pos_ == 'NOUN':
                    token_list_Noun.append(token.lemma_)
                    
        for token in doc2:
            if token.pos_ == 'VERB':
                    token_list_Verb.append(token.lemma_)
                    
        
        counts_Noun = Counter(token_list_Noun)
        counts_Verb = Counter(token_list_Verb)
        result_Noun = counts_Noun.most_common(20)
    #    result_Verb = counts_Verb.most_common(20)
    
    
    #---------------Nomen_Diagramm-------------
        values_Noun = [];
        counts_Noun = [];
        print(counts_Noun)
        for token in result_Noun:
            values_Noun.append(token[0]);
            counts_Noun.append(token[1]);
        
        hover = HoverTool(tooltips=[('Parameter', '@y'),( 'Description: ', '@x' )]) 
        
        source = ColumnDataSource(data=dict(
            x= values_Noun,
            y= counts_Noun,
            desc=values_Noun,
        ))
    
    
        grey_tone = "lightgrey"
        background_color = RGB(60, 60, 60)
        
        output_file("Export_Diagram_Noun.html")
        x = list(values_Noun)
        y = list(counts_Noun)
        patent_count_plot = figure(title = 'autonomous vehicles - Nomen', plot_width = 1000, plot_height = 800,
                                  x_range=x,y_range=(0, max(y)),tools = [hover])
        patent_count_plot.vbar(x = 'x', source = source , width=0.5, bottom=0, top = 'y', color="coral")
        
        patent_count_plot.xaxis.major_label_orientation = 0.75
        patent_count_plot.background_fill_color  = background_color
        patent_count_plot.xgrid.grid_line_color = grey_tone
        patent_count_plot.xaxis.major_label_text_font_size = '12pt'
        patent_count_plot.yaxis.major_label_text_font_size = '12pt'
        patent_count_plot.ygrid.grid_line_color= grey_tone
        patent_count_plot.border_fill_color = background_color
        patent_count_plot.yaxis.major_label_text_color = grey_tone
        patent_count_plot.xaxis.major_label_text_color = grey_tone
        patent_count_plot.xaxis.axis_line_color = grey_tone
        patent_count_plot.yaxis.axis_line_color = grey_tone
        patent_count_plot.xaxis.major_tick_line_color = grey_tone
        patent_count_plot.yaxis.major_tick_line_color = grey_tone
        patent_count_plot.xaxis.major_label_orientation = 0.7
        patent_count_plot.title.text_color = 'white'
        
        return patent_count_plot
    
    ##---------------Verben_Diagramm-------------
    #values_Verb = [];
    #counts_Verb = [];
    #
    #for token in result_Verb:
    #    values_Verb.append(token[0]);
    #    counts_Verb.append(token[1]);
    #    
    #
    #
    #
    #output_file("Export_Diagram_Verb.html")
    #x = list(values_Verb)
    #y = list(counts_Verb)
    #source = ColumnDataSource(data=dict(
    #    x= values_Noun,
    #    y= counts_Noun,
    #    desc=values_Noun,
    #))
    #
    #
    #patent_count_plot = figure(title = 'autonomous vehicles - Verben', plot_width = 1000, plot_height = 800,
    #                          x_range=x,y_range=(0, max(y)),tools = [hover])
    #patent_count_plot.vbar(x = 'x', source = source , width=0.5, bottom=0, top = 'y', color="limegreen")
    #
    #patent_count_plot.xaxis.major_label_orientation = 0.75
    #patent_count_plot.background_fill_color  = background_color
    #patent_count_plot.xgrid.grid_line_color = grey_tone
    #patent_count_plot.ygrid.grid_line_color= grey_tone
    #patent_count_plot.border_fill_color = background_color
    #patent_count_plot.xaxis.major_label_text_font_size = '12pt'
    #patent_count_plot.yaxis.major_label_text_font_size = '12pt'
    #patent_count_plot.yaxis.major_label_text_color = grey_tone
    #patent_count_plot.xaxis.major_label_text_color = grey_tone
    #patent_count_plot.xaxis.axis_line_color = grey_tone
    #patent_count_plot.yaxis.axis_line_color = grey_tone
    #patent_count_plot.xaxis.major_tick_line_color = grey_tone
    #patent_count_plot.yaxis.major_tick_line_color = grey_tone
    #patent_count_plot.xaxis.major_label_orientation = 0.7
    #patent_count_plot.title.text_color = 'white'
    #
    #save(patent_count_plot)
    #show(patent_count_plot)