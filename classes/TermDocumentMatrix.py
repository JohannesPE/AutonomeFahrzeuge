#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 17:31:26 2018

@author: johannespe
"""
import spacy
import pandas as pd
from collections import Counter, defaultdict
from bokeh.models import ColumnDataSource, Title, HoverTool
from bokeh.plotting import figure
from bokeh.colors import RGB
from spacy.tokens import Token, Span
import numpy as np


class TermDocumentMatrix:
    def do(patents,nlp):
        global largest
        global stacked_freq
        global top_assignees

        list_values = []

        for doc in patents.assignee_organization:
            if len(doc) > 35:
                list_values.append(doc[:35] + "...")
            else:
                list_values.append(doc) 
        
        patents['assignee_organization_gekuerzt'] = list_values
        
        def freq_counter(df, col):
            pos_counts = defaultdict(Counter)
            for index,value in patents_agg.iterrows():
                doc = nlp(value[col])
                for token in doc:
                    if token.pos_ in ['NOUN']:
                        pos_counts[index][token.lemma_] += 1
            frequency_df = pd.DataFrame(pos_counts)
            return frequency_df
                        
        patents_agg = patents.groupby('assignee_organization_gekuerzt').agg({'patent_abstract': ''.join, 
                                                   'assignee_organization_gekuerzt': 'first'})

        patents = patents.drop_duplicates(subset=['patent_number'], keep = "first")
            
        # Funktion ausführen und Term-Dokumenten-Matrix berechnen
        patents_freq = freq_counter(patents_agg, 'patent_abstract')
    
        # Unternehmen mit den meisten Patenten ermitteln
        top_assignees = patents.groupby('assignee_organization_gekuerzt').count()['patent_type'].nlargest(20)
        patents_freq_subset = patents_freq[top_assignees.index]

        
        # Meistgenannten Begriffe finden
        value_dict = defaultdict(Counter)
        for column in patents_freq_subset:
            for i,value in enumerate(patents_freq_subset[column].nlargest(2)):
                value_dict[column][patents_freq_subset[column].nlargest(2).index[i]] = value
        largest_each_comp = pd.DataFrame(value_dict).fillna(0)
        
        stacked_freq = patents_freq_subset.stack().reset_index()
        stacked_freq.columns = ['Begriff', 'Anmelder', 'Anzahl']
        stacked_freq = stacked_freq[stacked_freq['Anzahl'] > 1.0]
                
        largest = largest_each_comp

    
    def getTermDocumentMatrix(width,height,color,line_color,background_color):        
        #stacked_freq['Size'] = ((stacked_freq['Anzahl'] * 30/ stacked_freq['Anzahl'].max()) * 1.8)
        #stacked_freq['Anzahl'] = round(stacked_freq['Anzahl'])
        #source = ColumnDataSource(stacked_freq)
        
        stacked_freq['Size'] = ((stacked_freq['Anzahl'] * 30/ stacked_freq['Anzahl'].max()) * 5)

        
        source = ColumnDataSource(data=dict(
            x = stacked_freq['Begriff'],
            y = stacked_freq['Anmelder'],
            Anzahl = round(stacked_freq['Anzahl']),
            Size = stacked_freq['Size'],
        ))
        
        
        hover = HoverTool(tooltips=[
            ("Anzahl", "@Anzahl"),
            ("Anmelder", "@y"),
            ("Begriff", "@x")
        ])

        
        
        tools = [hover]
        # x_range, y_range sind so alphabetisch sortiert
        plot = figure(plot_width=width, plot_height=height, 
                         x_range = largest.index.tolist(), 
                         y_range = top_assignees.index.tolist(),
                         tools=tools
                        )
        
        # Kreise hinzufügen
        plot.circle(x = 'x', y = 'y', size = 'Size',
                       source = source, 
                       alpha = 2, color = color, line_alpha = 1, 
                       line_dash = 'solid', line_width = 3)
        
        # Design anpassen
        plot.background_fill_color  = background_color
        plot.xgrid.grid_line_color = line_color
        plot.ygrid.grid_line_color= line_color
        plot.border_fill_color = background_color
        plot.yaxis.major_label_text_color = line_color
        plot.xaxis.major_label_text_color = line_color
        plot.xaxis.axis_line_color = line_color
        plot.yaxis.axis_line_color = line_color
        plot.xaxis.major_tick_line_color = line_color
        plot.yaxis.major_tick_line_color = line_color
        plot.xaxis.major_label_orientation = 0.75
        plot.xaxis.major_label_text_font_size = '12pt'
        plot.yaxis.major_label_text_font_size = '12pt'
        return(plot)
        
    