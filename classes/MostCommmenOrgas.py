#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 12:30:04 2018

@author: johannespe
"""
#---------------Import---------------------
import pandas as pd
from collections import Counter
from bokeh.plotting import figure
from bokeh.colors import RGB
from bokeh.models import ColumnDataSource, Title, HoverTool


class MostCommmenOrgas: 
    def do(data,nlp):
        global x
        global y
        global source 
        
        data = data.drop_duplicates(subset=['patent_number'], keep = "first")
        
        top_assignees = data.groupby('assignee_organization').count()['patent_type'].nlargest(20)
        data=pd.DataFrame(top_assignees)
        data=data.unstack().reset_index()
        data.columns=["patent_type","assignee_organization","Anzahl"]
        
        #---------------Orgas_Anzahl und Orgas_Values trennen-------------
        values_Orgas = [];
        counts_Orgas = [];
        
        for doc in data.assignee_organization:
             values_Orgas.append(doc);
         
        for tok in data.Anzahl:
             counts_Orgas.append(tok);
            
        
        #output_file("Export_Diagram_Noun.html")
        x = list(reversed(values_Orgas))
        y = list(reversed(counts_Orgas))
        
        
        source = ColumnDataSource(data=dict(x= counts_Orgas, y= values_Orgas, desc=values_Orgas,))
    
    def getOrgasPlot(width,height,color,line_color,background_color):
        
        #Hover erzeugen
        hover = HoverTool(tooltips=[('Anzahl von Patenten', '@x'),( 'Unternehmen: ', '@y' )]) 
    
      
        #Figure erzeugen
        patent_count_plot = figure(plot_width = width, plot_height = height,
                                  x_range = (0, max(y)), y_range = x ,tools = [hover])
        
        #patent_count_plot.vbar(x = 'y', source = source , width=0.5, bottom=0, top = 'x', color=color)  
        patent_count_plot.hbar(y="y", right="x", left=0, height=0.5, source=source,color = color)
        #Designänderungen
        patent_count_plot.background_fill_color  = background_color
        patent_count_plot.xgrid.grid_line_color = line_color
        patent_count_plot.ygrid.grid_line_color= line_color
        patent_count_plot.border_fill_color = background_color
        patent_count_plot.yaxis.major_label_text_color = line_color
        patent_count_plot.xaxis.major_label_text_color = line_color
        patent_count_plot.xaxis.axis_line_color = line_color
        patent_count_plot.yaxis.axis_line_color = line_color
        patent_count_plot.xaxis.major_tick_line_color = line_color
        patent_count_plot.yaxis.major_tick_line_color = line_color
        patent_count_plot.title.text_color = 'white'
        patent_count_plot.xaxis.major_label_text_font_size = '15pt'
        patent_count_plot.yaxis.major_label_text_font_size = '12pt'
    
        return patent_count_plot