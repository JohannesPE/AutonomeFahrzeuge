#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 23 17:19:27 2018

@author: johannespe
"""
#---------------Import---------------------
import spacy 
import pandas as pd
from bokeh.models import ColumnDataSource, Title, HoverTool
from bokeh.io import output_file, save, show
from bokeh.plotting import figure
from bokeh.colors import RGB
from collections import Counter
from numpy import pi
from decimal import Decimal    

class NomenVerbenExtract:        
    def do(data,nlp):
        global hover
        global values_Noun
        global counts_Noun
        global values_Verb
        global counts_Verb
        
        data = data.drop_duplicates(subset=['patent_number'], keep = "first")
        
        joined_text2 = ' '.join(data.patent_abstract.tolist())
        doc = nlp(joined_text2)
        
        new_string = ''.join([tok.text_with_ws for tok in doc if tok.is_stop == False])
        doc2 = nlp(new_string)
                
        token_list_Noun = []
        token_list_Verb = []
        
        for token in doc2:
            if token.pos_ == 'NOUN':
                    token_list_Noun.append(token.lemma_)
                    
        for token in doc2:
            if token.pos_ == 'VERB':
                    token_list_Verb.append(token.lemma_)
                    
        
        counts_Noun = Counter(token_list_Noun)
        counts_Verb = Counter(token_list_Verb)
        result_Noun = counts_Noun.most_common(20)
        result_Verb = counts_Verb.most_common(20)
        
        #---------------Nomen-------------
        values_Noun = [];
        counts_Noun = [];
        for token in result_Noun:
            values_Noun.append(token[0]);
            counts_Noun.append(token[1]);
        
        #---------------Verben-------------
        values_Verb = [];
        counts_Verb = [];
        
        for token in result_Verb:
            values_Verb.append(token[0]);
            counts_Verb.append(token[1]);
           
        
        #----Hover
        hover = HoverTool(tooltips=[('Parameter', '@y'),( 'Description: ', '@x' )]) 
        
        
    def getNomen(width,height,color,line_color,background_color):
        source = ColumnDataSource(data=dict(
            x= values_Noun,
            y= counts_Noun,
            desc=values_Noun,
        ))


        x = list(values_Noun)
        y = list(counts_Noun)
        patent_count_plot = figure(title = 'autonomous vehicles - Nomen', plot_width = width, plot_height = width,
                                  x_range=x,y_range=(0, max(y)),tools = [hover])
        patent_count_plot.vbar(x = 'x', source = source , width=0.5, bottom=0, top = 'y', color=color)
        
        patent_count_plot.xaxis.major_label_orientation = 0.75
        patent_count_plot.background_fill_color  = background_color
        patent_count_plot.xgrid.grid_line_color = line_color
        patent_count_plot.xaxis.major_label_text_font_size = '12pt'
        patent_count_plot.yaxis.major_label_text_font_size = '12pt'
        patent_count_plot.ygrid.grid_line_color= line_color
        patent_count_plot.border_fill_color = background_color
        patent_count_plot.yaxis.major_label_text_color = line_color
        patent_count_plot.xaxis.major_label_text_color = line_color
        patent_count_plot.xaxis.axis_line_color = line_color
        patent_count_plot.yaxis.axis_line_color = line_color
        patent_count_plot.xaxis.major_tick_line_color = line_color
        patent_count_plot.yaxis.major_tick_line_color = line_color
        patent_count_plot.xaxis.major_label_orientation = 0.7
        patent_count_plot.title.text_color = 'white'
        
        return patent_count_plot
    
    def getVerben(width,height,color,line_color,background_color):
        x = list(values_Verb)
        y = list(counts_Verb)
        source = ColumnDataSource(data=dict(
            x= values_Verb,
            y= counts_Verb,
            desc=values_Verb,
        ))

        
        patent_count_plot = figure(title = 'autonomous vehicles - Verben', plot_width = width, plot_height = width,
                                  x_range=x,y_range=(0, max(y)),tools = [hover])
        patent_count_plot.vbar(x = 'x', source = source , width=0.5, bottom=0, top = 'y', color=color)
        
        patent_count_plot.xaxis.major_label_orientation = 0.75
        patent_count_plot.background_fill_color  = background_color
        patent_count_plot.xgrid.grid_line_color = line_color
        patent_count_plot.ygrid.grid_line_color= line_color
        patent_count_plot.border_fill_color = background_color
        patent_count_plot.xaxis.major_label_text_font_size = '12pt'
        patent_count_plot.yaxis.major_label_text_font_size = '12pt'
        patent_count_plot.yaxis.major_label_text_color = line_color
        patent_count_plot.xaxis.major_label_text_color = line_color
        patent_count_plot.xaxis.axis_line_color = line_color
        patent_count_plot.yaxis.axis_line_color = line_color
        patent_count_plot.xaxis.major_tick_line_color = line_color
        patent_count_plot.yaxis.major_tick_line_color = line_color
        patent_count_plot.xaxis.major_label_orientation = 0.7
        patent_count_plot.title.text_color = 'white'
        
        return patent_count_plot
    
    def getNomenPie(width,height,color,line_color,background_color):
        summe = 0;
        for tok in counts_Noun:
            summe = summe + tok;    
        

        percentages = [0]
        last=0
        for tok in counts_Noun:
            last=last+tok / summe
            percentages.append(round(last,2))
    
        # define starts/ends for wedges from percentages of a circle
        percents = percentages
        starts = [p*2*pi for p in percents[:-1]]
        ends = [p*2*pi for p in percents[1:]]
        
        # a color for each pie piece
        color = []
        i = 1;
        while i < len(percentages):
            if i%2 == 0:
                color.append("lightgrey")
                i = i + 1
            else:
                color.append("yellow")
                i = i + 1
        
        p = figure(x_range=(-1,1), y_range=(-1,1))
        p.wedge(x=0, y=0, radius=1, start_angle=starts, end_angle=ends, color=color)
        
        return p 