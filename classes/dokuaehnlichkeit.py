#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 23 17:19:27 2018

@author: johannespe
"""
#---------------Import---------------------
import spacy
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, HoverTool
from collections import Counter
from sklearn.metrics.pairwise import cosine_similarity 

#---------------Bokeh-Plot-------------
class dokuaehnlichkeit:
    def do(data):
        global source
        global data_agg
        
        data = data.drop_duplicates(subset=['patent_number'], keep = "first")
        
        #---------------Top 20 patente-------------
        counts = Counter(data['assignee_organization'].tolist())
        results = counts.most_common(20)
        
        
        list_values = []
        for doc in data.assignee_organization:
            if len(doc) > 35:
                list_values.append(doc[:35] + "...")
            else:
                list_values.append(doc)
        
        data['assignee_organization_gekuerzt'] = list_values
        data = data.drop(['inventor_first_name','inventor_last_name', 'patent_type','patent_number'], axis = 1)
        counts = Counter(data['assignee_organization_gekuerzt'].tolist())
        results = counts.most_common(20)
        
        
        
        #---------------Vektorisieren-------------
        data_agg = data.groupby('assignee_organization_gekuerzt').agg({'patent_abstract': ' '.join,
                                                     'assignee_organization_gekuerzt' : 'first'})
        data_agg = data_agg.loc[data_agg['assignee_organization_gekuerzt'].isin([t[0] for t in results])]
        
        
        
      
        
        vec = CountVectorizer(stop_words = ['is', 'are', 'will', 'the', 'an', 'on', 'and', 'of', 'for',
                                                  'in','to','or'], min_df = 0.3)
        
        tdm = vec.fit_transform(data_agg['patent_abstract']).toarray()
        similarity = cosine_similarity(tdm)
        
        
        #---Data Frame 
        df = pd.DataFrame(similarity)
        df.columns = list(data_agg['assignee_organization_gekuerzt'])
        df["companies"] = list(data_agg['assignee_organization_gekuerzt'])
        df = df.set_index("companies")
        df = df.unstack().reset_index()
        df.columns = ["Unternehmen_1","Unternehmen_2","Aehnlichkeit"]
        
        
        source = ColumnDataSource(data=dict(
            x = df['Unternehmen_1'],
            y = df['Unternehmen_2'],
            wert = df['Aehnlichkeit'] * 6.28,
            wert_Original = df['Aehnlichkeit'] * 100,
        ))
        
        
    def getDa(width,height,color,line_color,background_color):
        #Hover erzeugen
        hover = HoverTool(tooltips=[('Patent-Y', '@y'),( 'Patent-X: ', '@x' ),
                            ('Übereinstimmung','@wert_Original' + ' %')])
        

        #Figure erzeugen
        p = figure( plot_width = width, plot_height = height,
                   x_range =data_agg["assignee_organization_gekuerzt"].tolist() , y_range= data_agg["assignee_organization_gekuerzt"].tolist(),
                   tools = [hover])
        
        p.wedge(x = 'x', y = 'y', radius = 0.3,
                            start_angle=0, end_angle= 'wert', color=color, alpha=1, source=source)


        #Design
        p.background_fill_color  = background_color
        p.xgrid.grid_line_color = line_color
        p.ygrid.grid_line_color= line_color
        p.border_fill_color = background_color
        p.yaxis.major_label_text_color = line_color
        p.xaxis.major_label_text_color = line_color
        p.xaxis.axis_line_color = line_color
        p.yaxis.axis_line_color = line_color
        p.xaxis.major_tick_line_color = line_color
        p.yaxis.major_tick_line_color = line_color
        p.xaxis.major_label_orientation = 0.6
        p.xaxis.major_label_text_font_size = '12pt'
        p.yaxis.major_label_text_font_size = '12pt'
        return p