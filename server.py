from flask import Flask, render_template
from bokeh.embed import components
from bokeh.resources import INLINE
from bokeh.colors import RGB
import pandas as pd
import spacy 

from classes.MostCommmenOrgas import MostCommmenOrgas as MCO
from classes.NomenVerbenExtract import NomenVerbenExtract as NVE
from classes.TermDocumentMatrix import TermDocumentMatrix as TDM
from classes.dokuaehnlichkeit import dokuaehnlichkeit as DA



#---------------Pipelining------------
nlp = spacy.load('en')
file = open('files/stopwords.txt','r') 
for line in file:
   nlp.vocab[line].is_stop = True
file.close()


data = pd.read_csv('files/Organisation.csv', delimiter=',')

data.assignee_organization.replace('None', data.inventor_first_name 
                                     + ' ' + data.inventor_last_name, 
                                    inplace = True)

#---------Text Analyse
MCO.do(data,nlp)
print("MostCommonOrgas geladen")

#NomenVerbenExtract
print("NomenVerbenExtract wird geladen...")
NVE.do(data,nlp)
print("NomenVerbenExtract fertig")

#TermDocumentMatrix
print("TermDocumentMatrix wird geladen...")
TDM.do(data,nlp)
print("TermDocumentMatrix geladen")

#Dokumentenähnlichkeit
print("Dokumentenähnlichkeit wird geladen...")
DA.do(data)
print("Dokumentenähnlichkeit geladen")


#---Farbe der Plots
color="white"
line_color="yellow"
background="#0D424B"



app = Flask(__name__)    
print("Website ist nun verfügbar")



@app.route('/')
def index():
    plot = MCO.getOrgasPlot(950,650,line_color,color,background)
    
    # Embed plot into HTML via Flask Render
    script, div = components(plot)
    
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()
    
    return render_template("index.html", script=script, div=div, js_resources=js_resources,
        css_resources=css_resources)
 
@app.route('/NomenVerben')
def nomenVerben(): 
    # Create the plot
    nPlot = NVE.getNomen(600,500,line_color,color,background)
    # Embed plot into HTML via Flask Render
    nScript, nDiv = components(nPlot)
    
    # Create the plot
    vPlot = NVE.getVerben(600,500,line_color,color,background)
    # Embed plot into HTML via Flask Render
    vScript, vDiv = components(vPlot)
    
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()
    
    return render_template("NomenVerben.html",nPlot=nScript, nDiv=nDiv,vPlot=vScript,vDiv=vDiv, js_resources=js_resources,
        css_resources=css_resources)    
    

@app.route('/tdm')
def tdm():
    # Create the plot
    plot=TDM.getTermDocumentMatrix(1250,700,line_color,color,background)
 
    # Embed plot into HTML via Flask Render
    script, div = components(plot)
    
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()
    
    return render_template("tdm.html", script=script, div=div,  js_resources=js_resources,
        css_resources=css_resources)
    
@app.route('/da')
def da():
    # HIER DOCUMENTEN AEHNLICHKEIT
    plot=DA.getDa(1250,850,line_color,color,background)
 
    # Embed plot into HTML via Flask Render
    script, div = components(plot)

    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()
    
    return render_template("DokuAehnlichkeit.html", script=script, div=div,  js_resources=js_resources,
        css_resources=css_resources)

@app.route('/fazit')
def fazit():
    return render_template("fazit.html")

# With debug=True, Flask server will auto-reload when there are code changes
if __name__ == '__main__':
	app.run(port=5000)